#include <stdio.h>

int main() {
    int i, x[6], sum = 0;

    printf("Enter 6 numbers: ");

    for(i = 0; i < 6; ++i) {
        // Equivalent to scanf("%d", &x[i]);
        // Here we take input from the user that expects an int, and stores that inputted
        // value into the address of x[i]...the syntax here is somewhat strange
        scanf("%d", x+i);

        // Equivalent to sum += x[i];
        // Here we use a pointer to the value of x[i] and add it to the variable, sum
        sum += *(x+i);
    }

    printf("Sum = %d\n", sum);

    return 0;
}

// Sample Output:
// Enter 6 numbers: 12
// 14
// 13
// 22
// 16
// 19
// Sum = 96

// In simple words, array names are converted to pointers. That's the reason why you can use pointers to access elements of arrays. However, you should remember that pointers are arrays are not the same.

// There are a few cases where array names don't decay to pointers (see: https://stackoverflow.com/questions/17752978/exceptions-to-array-decaying-into-a-pointer)
