#include <stdio.h>

int main()
{
    int var = 5;
    printf("var: %d\n", var);

    // Notice the use of & before var
    printf("address of var: %p\n", &var);
    return 0;

    // returns:
    // var: 5
    // address of var: 0x7ffd455236a4
    // NOTE: The address will change every time you compile this program because it is allocating the value of var to a different memory address (&var)
}
