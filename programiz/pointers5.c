#include <stdio.h>

int main()
{
    int* pc, c, d;
    c = 5;
    d = -15;

    pc = &c;
    printf("The pointer pc, is currently equal the address of the variable, c, which has the address of: %p\n", pc);
    printf("The pointer pc, is currently equal to the address of the variable, c, which has the value of: %d\n", *pc);

    pc = &d;
    printf("The pointer pc, is now reassigned to the memory address of d\n");
    printf("The pointer pc, is currently equal the address of the variable, d, which has the address of: %p\n", pc);
    printf("The pointer pc, is currently equal to the address of the variable, d, which has the value of: %d\n", *pc);
}
