// taken from https://www.programiz.com/c-programming/c-file-input-output

#include <stdio.h>
#include <stdlib.h>

// Program that reads a text file and outputs the result to the console

int main()
{
    int num;
    FILE *fptr;

    fptr = fopen("/home/brian/Documents/Code/book_tutorials/effective_c/programiz/program.txt", "r");

    if (fptr == NULL)
    {
        printf("Error! openinig file");

        exit(1);
    }

    // fscanf is the only new function presented here from this tutorial series thus far
    // it takes the file pointer, reads an expected digit, and takes the memory address of num as arguments
    fscanf(fptr, "%d", &num);

    printf("value of n=%d\n", num);
    fclose(fptr);

    return 0;
}

// Outputs:
// value of n=8
