// taken from https://www.programiz.com/c-programming/c-file-input-output

// note this file is used in conjunction with read_from_binary_file.c and program.bin

#include <stdio.h>
#include <stdlib.h>

// first we create a structure(object), that simply has three  variables expecting integers called n1, n2, and n3.
struct threeNum
{
    int n1, n2, n3;
};

int main()
{
    // we then instantiate a counter for for loop called num
    // threeNum is then used as a prototype for num
    // and a filepointer is also created called fptr
    int n;
    struct threeNum num;
    FILE *fptr;

    // note the syntax of "wb" for "write binary"
    fptr = fopen("/home/brian/Documents/Code/book_tutorials/effective_c/programiz/program.bin", "wb");

    // simple error check to be sure the file exists
    if (fptr == NULL)
    {
        printf("Error! opening file");

        exit(1);
    }

    // for loop that counts four times..
    for (n = 1; n < 5; ++n)
    {
        // and assigns the counted value to n
        // which is then manipulated mathematically in the following ways:
        num.n1 = n;
        num.n2 = 5*n;
        num.n3 = 5*n + 1;

        // note the difference between this and the write_to_text_file.c syntax, instead of printf, and scanf, we use fwrite..
        // which takes the address of the num, the size of the structure threeNum..
        
        // and 1, since we're only inserting 1 instance of num
        // and finally fptr, which points to the file we're writing to (program.bin)
        fwrite(&num, sizeof(struct threeNum), 1, fptr);
    }

    // and of course, we need to close the file before exiting our program
    fclose(fptr);

    return 0;
}
