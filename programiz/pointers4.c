#include <stdio.h>
// Changing Value Pointed by Pointers
int main()
{
    // create a pointer called pc, and a integer variable called c
    int* pc, c;
    // assign variable c the value of the integer, 5
    c = 5;
    // the pointer pc is then assigned the memory address of c
    pc = &c;
    // the pointer, pc is then reassigned the integer value, 1
    *pc = 1;
    // thusly, when we print the value of the pointer, pc, it also returns... 
    printf("%d\n", *pc); // returns 1
    // and also thusly, when we print the value of c, it returns...
    printf("%d\n", c); // returns 1

    // NOTE: The important thing to note here is that the memory address, &c, is referenced by both the variable, c, as well as its pointer pc.
    
    // If we had reassigned pc to 1 like so:
    // pc = 1;
    // Then we would have made pc a regular variable, and it would no longer point the memory address of c (c would still hold the value of 5 in this case)
    
    // But because we assigned the POINTER pc:
    // *pc = 1;
    // We reassigned the value of the pointer, and thusly simultaneous changed the value of the memory address &c
}
