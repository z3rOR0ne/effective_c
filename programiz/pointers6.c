#include <stdio.h>

int main()
{
    int c = 5;
    int *p = &c;

    printf("%d\n", *p); // 5
    return 0;

    // NOTE: If you look at common_mistakes.c, you may notice similar syntax to:
    // int *p = &c;
    // The reason this DOES NOT ERROR, is because this statement is equivalent to:
    // int *p;
    // p = &c;
    // In either case, we initialize a pointer called p.  The value of the p variable is then assigned the memory address of c.
}
