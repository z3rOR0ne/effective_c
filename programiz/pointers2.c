#include <stdio.h>

int main()
{
    // create a pointer called pc, and a integer variable called c
    int* pc, c;
    // assign variable c the value of the integer, 5
    c = 5;
    // the pointer pc is then assigned the memory address of c
    pc = &c;
    // print the value of the pointer pc
    printf("the value of what the pointer, pc, points to: %d\n", *pc);
    // prints the memory address of pointer pc
    printf("the memory address of the pointer pc: %p\n", pc);

    return 0; // returns 5
}
