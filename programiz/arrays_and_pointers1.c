#include <stdio.h>

int main() {
    int x[4];
    int i;

    for(i = 0; i < 4; ++i) {
        printf("&x[%d] = %p\n", i, &x[i]);
    }

    printf("Address of array x: %p\n", x);

    return 0;
}

// Outputs:
// &x[0] = 0x7ffe619a24e0
// &x[1] = 0x7ffe619a24e4
// &x[2] = 0x7ffe619a24e8
// &x[3] = 0x7ffe619a24ec
// Address of array x: 0x7ffe619a24e0
//
// Notice that the address of &x[0] and x is the same. It's because the variable name x points to the first element of the array.
//
// Also notice that the difference of 4 bytes between two consecutive elements of array x. It is because the size of int is 4 bytes (on our compiler).
