// This is simply to demonstrate common misconceptions when working with pointers:

// Simply initialize a variable called c, and a pointer called pc
int c, *pc;

// ERROR: pc is address but c is not
pc = c;
// ERROR: &c is an address, but *pc is not
*pc = &c;
// both &c and pc are addresses, NOT ERROR
pc = &c;
// both c and *pc are values, NOT ERROR
*pc = c;
