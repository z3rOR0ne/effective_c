#include <stdio.h>
// Changing Value Pointed by Pointers
int main()
{
    // create a pointer called pc, and a integer variable called c
    int* pc, c;
    // assign variable c the value of the integer, 5
    c = 5;
    // the pointer pc is then assigned the memory address of c
    pc = &c;
    // the variable c is then reassigned the value of the integer, 1
    c = 1;
    // thusly, when we print the value of c, it returns...
    printf("%d\n", c); // returns 1
    // and also thusly, when we print the value of the pointer, pc, it also returns... 
    printf("%d\n", *pc); // returns 1
}
