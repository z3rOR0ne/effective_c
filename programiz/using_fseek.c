// taken from https://www.programiz.com/c-programming/c-file-input-output

#include <stdio.h>
#include <stdlib.h>

struct threeNum
{
    int n1, n2, n3;
};

int main()
{
    int n;
    struct threeNum num;
    FILE *fptr;

    fptr = fopen("/home/brian/Documents/Code/book_tutorials/effective_c/programiz/program.bin", "rb");

    if (fptr == NULL)
    {
        printf("Error! opening file");
        exit(1);
    }

    // Moves the cursor to the end of the file
    fseek(fptr, -sizeof(struct threeNum), SEEK_END);

    for (n = 1; n < 5; ++n)
    {
        fread(&num, sizeof(struct threeNum), 1, fptr);
        printf("n1: %d\tn2: %d\tn3: %d\n", num.n1, num.n2, num.n3);
        fseek(fptr, -2*sizeof(struct threeNum), SEEK_CUR);
    }
    fclose(fptr);

    return 0;

}

// Output after compiling read_from_binary.cc (and write_to_binary.c):
// n1: 1	n2: 5	n3: 6
// n1: 2	n2: 10	n3: 11
// n1: 3	n2: 15	n3: 16
// n1: 4	n2: 20	n3: 21

// Output after compiling using_fseek.c:
// n1: 4	n2: 20	n3: 21
// n1: 3	n2: 15	n3: 16
// n1: 2	n2: 10	n3: 11
// n1: 1	n2: 5	n3: 6

