#include <stdio.h>
#include <stdlib.h>

// used in conjunction with write_to_binary_file.c and program.bin

// Note how we need threeNum to be reinstantiated here, this is so that we have an expectation of the size of threeNum later on...
struct threeNum
{
    int n1, n2, n3;
};

int main()
{
    // extremely similar setup to write_to_binary_file.c
    int n;
    struct threeNum num;
    FILE *fptr;

    fptr = fopen("/home/brian/Documents/Code/book_tutorials/effective_c/programiz/program.bin", "rb");

    if (fptr == NULL)
    {
        printf("Error! opening file");

        exit(1);
    }

    for (n = 1; n < 5; ++n)
    {
        // pretty much the same syntax as fwrite() function in write_to_binary_file.c
        fread(&num, sizeof(struct threeNum), 1, fptr);
        // except here we use printf to take the ouput of fread and print it out to the console
        printf("n1: %d\tn2: %d\tn3: %d\n", num.n1, num.n2, num.n3);
    }

    // always close your files!
    fclose(fptr);

    return 0;
}

// Outputs:
// n1: 1	n2: 5	n3: 6
// n1: 2	n2: 10	n3: 11
// n1: 3	n2: 15	n3: 16
// n1: 4	n2: 20	n3: 21

