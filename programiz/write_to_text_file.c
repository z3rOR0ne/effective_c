// taken from https://www.programiz.com/c-programming/c-file-input-output

#include <stdio.h>
#include <stdlib.h>

// Simple C program that takes the input from the user, and writes the output to the file program.txt

int main()
{
    // initialize a variable called num
    int num;
    // FILE should always use a pointer, NEVER work with the FILE object directly, here we create a pointer called fptr
    FILE *fptr;

// fopen will open the text content of whatever text file we tell it and in this case stores it in the pointer fptr
    fptr = fopen("/home/brian/Documents/Code/book_tutorials/effective_c/programiz/program.txt", "w");

    // Simple if check to be sure that the file stored in fptr exists
    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    // Here we ask for user input to be stored in memory, expecting a digit as input and stores it in the memory address of num variable
    printf("Enter num: ");
    scanf("%d", &num);

    // fprintf, takes the fptr as the fptr as the file location to be written to, expecting a digit, followed by a new line, and then puts the value of the variable num as input.
    fprintf(fptr, "%d\n", num);
    // make sure to invoke fclose() to close the file once done.
    fclose(fptr);

    // return success code.
    return 0;
}

// after compiling...
// test by now invoking cat on program.txt
// cat program.txt
