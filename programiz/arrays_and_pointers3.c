#include <stdio.h>
int main() {
    
    int x[5] = {1, 2, 3, 4, 5};
    int* ptr;

    // ptr is assigned the address of the third element
    ptr = &x[2];

    printf("*ptr = %d \n", *ptr); // 3
    printf("*(ptr+1) = %d \n", *(ptr+1)); // 4
    printf("*(ptr-1) = %d \n", *(ptr-1)); // 2

    return 0;
}

// Outputs:
// *ptr = 3 
// *(ptr+1) = 4 
// *(ptr-1) = 2 

// Hopefully this explains the concept of how pointers are related to arrays.  Here we are able to grab the values within the array x, by first creating a pointer ptr, and then assigning it the value of the third element of the array x:
// ptr = &x[2];
// Then we are able to actually reference the various other elements within the array by iterating over it using the syntax above
// i.e.
// *(ptr+1)
// *(ptr-1)
