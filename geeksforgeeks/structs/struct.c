// https://www.geeksforgeeks.org/structures-c/

#include <stdio.h>

// Structs are a data type not seen in higher level languages like javascript or python, but can be thought of in some ways as likeObjects or Classes. Due to the requirements of memory allocation, C requires that any values allocated to variables within a struct to be allocated AFTER instantiated.  Thusly the following instantiated a series of arrays which expect characters, and one integer, without actually ASSIGNING values to it..

struct address
{
    char name[50];
    char street[100];
    char city[50];
    char state[20];
    int pin;
};

// Here are some more examples of struct declaration:

// A variable declaration with structure declaration.
struct Point
{
int x, y;
} p1; // The variable p1 is declared with 'Point'

// A variable declaration like basic data types
struct Point
{
    int x, y;
};

int main()
{
    struct Point p1; // The variable p1 is declared like a normal variable
}

// To demonstrate that a struct MUST instantiate all variables within it BEFORE ASSIGNMENT, the following code is provided

struct Point
{
    int x = 0;  // COMPILER ERROR: cannot initialize members here
    int y = 0;  // COMPILER ERROR: cannot initialize members here
};

// Note that the above code won't compile because there is NO MEMORY ADDRESS for x or y yet.  Below this is done correctly

struct Point
{
    int x, y;
};

int main()
{
    // A valid initialization, member x gets value 0 and y
    // gets value 1. The order of declaration is followed.
    struct Point p1 = {0, 1}
}

// You can now see how structs resemble objects or, more specifically, classes

struct Point
{
    int x, y;
};

int main()
{
 struct Point p1 = {0, 1};

 // Accessing members of point p1
 p1.x = 20;
 printf ("x = %d, y = %d\n", p1.x, p1.y);

 return 0;
}

// Output:
// x = 20, y = 1
// see struct2.c
