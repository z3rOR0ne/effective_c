// This file is simply here to demonstrate that the enum type requires that the constants be unique in their scope.
// The following example will throw an error rather than simply compile because the failed key is repeated in both enum state and enum result

enum state {working, failed};
enum result {failed, passed};

int main() { return 0; }

// returns:
// error: redeclaration of enumerator 'failed'
