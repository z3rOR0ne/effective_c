/* A signal is a software generated interrupt that is sent to a process by the OS because of when a user presses ctrl-c or another process tells something to this process.
 *
There are a fix set of signals that can be sent to a process. These signals are identified by integers.

Signal numbers have symbolic names. For example SIGCHLD is the number of the signal sent to the parent process when the child process terminates.*/

/* Examples: */

#define SIGHUP 1 /* Hangup the process */ 
#define SIGINT 2 /* Interrupt the process */
#define SIGQUIT 3 /* Quit the process */
#define SIGILL 4 /* Illegal instruction */
#define SIGTRAP 5 /* Trace trap */
#define SIGABRT 6 /* Abort */
