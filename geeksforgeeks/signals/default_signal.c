// CPP program to illustrate
// default Signal Handlers
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void handle_sigint(int sig)
{
    printf("Caught signal %d\n", sig);
}

int main()
{
    signal(SIGINT, handle_sigint); // note that as of right now, the program will NOT terminate with CTRL+C, it will simply print the statement above in handle_sigint(), to quit hit CTRL+\
    while (1)
    {
        printf("hello world\n");
        sleep(1);
    }
    return 0;
}
