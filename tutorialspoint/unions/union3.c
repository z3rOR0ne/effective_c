#include <stdio.h>
#include <string.h>

union Data {
    int i;
    float f;
    char str[20];
};

int main() {
    
    union Data data;

    data.i = 10;
    printf("data.i : %d\n", data.i);

    data.f = 220.5;
    printf("data.f : %f\n", data.f);

    strcpy(data.str, "C Programming");
    printf("data.str : %s\n", data.str);

    return 0;
}

// Outputs:
// data.i : 10
// data.f : 220.500000
// data.str : C Programming
//
// NOTE:  The reason this works and union2.c does NOT, is because the calls to the memory address consistently are done one at a time. The memory address of data is changed when its different properties are assigned values, but as long as the methods invoked that involve this particular property are executed BEFORE that union's other properties are assigned values, then they should find the appropriate memory address and therefore find the correct values. The short answer is simply that when one element of a union is assigned a value, the other values change because it is only the memory address that does not change, but all values at that memory address change when a union is created, this is unlike a struct (or an Object/Class in other languages)

