#include <stdio.h>
#include <string.h>

union Data {
    int i;
    float f;
    char str[20];
};

int main() {
    
    union Data data;

    data.i = 10;
    data.f = 220.5;
    strcpy(data.str, "C Programming");

    printf("data.i : %d\n", data.i);
    printf("data.f : %f\n", data.f);
    printf("data.str : %s\n", data.str);

    return 0;
}

// Outputs:
// data.i : 1917853763
// data.f : 4122360580327794860452759994368.000000
// data.str : C Programming


// NOTE:  See how the above code produces an unexpected result!  Why would data.i not be 10? Or data.f produce 220.5?  This is due to the fact that unions take up the same memory address. Thusly, when we reference them after with our printf() statements, the memory address of data is the same and is overwritten.  This is because printf statements overwrite the memory address of data.i each time.  This is why the last printf statement prints correctly (i.e. "C Programming"), because as the last call to printf() calls to the last memory address allocation: strcpy(data.str, "C Programming");
// See union3.c for the correct implementation of unions using the same elements.
