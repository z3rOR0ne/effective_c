#include <stdio.h>
#include <stdlib.h>

/* taken from https://iq.opengenus.org/character-in-c/ */

int main()
{
    char character = 'Z';
    printf("character = %c\n", character);
    printf("character = %d, Stored as integer\n", character);
    return 0;
}
