#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    // checks to see if ch is lower case
    char ch = 'z';
    if(islower(ch))
        printf("lower case\n");
    else
        printf("upper case\n");
    // returns 'lower case'

    // checks to see if ch2 is upper case
    char ch2 = 'Z';
    if(isupper(ch2))
        printf("upper case\n");
    else
        printf("lower case\n");
    // returns 'upper case'

    // checks to see if ch3 is in the alphabet
    char ch3 = 'z';
    if(isalpha(ch3))
        printf("alphabet\n");
    else
        printf("Numeric\n");
    // returns 'alphabet'

    // checks to see if ch4 is a digit
    char ch4 = '1';
    if(isdigit(ch4))
        printf("numeric\n");
    else
        printf("alphabet\n");
    // returns 'numeric'
    
    // checks to see if ch5 is a digit or in the alphabet
    char ch5 = '/';
    if(isalnum(ch5))
        printf("numeric or alphabet\n");
    else
        printf("other symbol\n");
    // returns 'other symbol'
    
    // checks to see if ch6 is a punctuation character
    char ch6 = '!';
    if(ispunct(ch6))
        printf("punctuation\n");
    else
        printf("other symbol\n");
    // returns 'punctuation'
    
    // checks to see if ch7 is a space
    char ch7 = ' ';
    if(isspace(ch7))
        printf("space\n");
    else
        printf("other symbol\n");
    // returns 'space'
    
    // converting character to lower or upper space
    char ch8 = 'A';
    ch8 = tolower(ch8);
    printf("%c\n", ch8); // prints a
    ch8 = toupper(ch8);
    printf("%c\n", ch8); // prints A

    // interesting example of how sizeof() method works, note that data type, char has a smaller size than the actual value of the character it holds
    char a = 'a';
    printf("Size of char : %d\n", sizeof(a)); // prints Size of char : 1
    printf("Size of char : %d\n", sizeof('a')); // prints Size of char: 4

    // NOTE: All the function in Ctype work under constant time

    return 0;
}

