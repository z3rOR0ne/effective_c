#include <stdio.h>

/* This program is used to demonstrate the sizeof() operator where the sizeof() operator will function with the expression and will have a return type as the regular expression itself after passing from the size of operator as a parameter. 
 * https://www.educba.com/sizeof-in-c/
 * */

int main()
{
    int p = 15;
    float f = 18.20;
    int q = 32;
    double r = 10.34;
    printf("Size of Regular expression becomes %lu\n", sizeof(p + (f - q)*r));
    return 0;
}

/* Outputs: 
 * Size of Regular expression becomes 8 */
