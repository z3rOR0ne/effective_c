// Taken from https://www.educba.com/unsigned-int-in-c/

#include <stdio.h>

int main(void)
{
int a = 57;
unsigned int b = (unsigned int)a;
printf("The value of signed variable is: %u\n",a);
printf("The value of unsigned variable is: %u\n",b);
return 0;
}

// returns
// The value of signed variable is: 57
// The value of unsigned variable is: 57

