#include <stdio.h>

/* This program is sued to find the size of the array dynamically using sizeof() operator which is used dynamically allocating the memory to the elements within the array as shown in the output. 
 * https://www.educba.com/sizeof-in-c/
*/

int main()
{
    int arr[] = { 10, 19, 24, 0, 6, 42, 78, 60 };
    printf("Total elements of array list :%u\n", sizeof(arr) / sizeof(arr[5]));
    return 0;
}

/* Outputs: 
 * Total elements of array list :8 */
