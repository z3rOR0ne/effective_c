#include <stdio.h>

/* simple demonstration of sizeof() function in C 
https://www.educba.com/sizeof-in-c
*/

int main() {
    int m = 80;
    float n = 5.2;
    printf("size of int becomes: %d\n", sizeof(m));
    printf("size of float becomes %f\n", sizeof(n));
    printf("size of char becomes: %ld\n", sizeof(char));
    return 0;
}

/* Output:
size of int becomes: 4
size of float becomes 0.000000
size of char becomes: 1
*/
