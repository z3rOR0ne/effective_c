#include <stdio.h>

/* This program is used to demonstrate the sizeof() operator compatible with the variables assigned and then finding the number of bytes and values that is needed for allocation to the memory as shown in the output. 
 * https://www.educba.com/sizeof-in-c/
 * */

int main()
{
    char m_var1 = 26;
    int p_var2 = 'i';
    double o_var3 = 15.99;
    printf("size of the character variable assigned %c\n", sizeof(m_var1));
    printf("size of the integer variable assigned %d\n", sizeof(p_var2));
    printf("size of the double or float variable assigned %f\n", sizeof(o_var3));
    return 0;
}
/* Outputs: 
 * size of the character variable assigned 
 * size of the integer variable assigned 4 
 * size of the double or float variable assigned 0.000000 */

