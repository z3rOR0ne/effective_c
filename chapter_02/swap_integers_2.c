#include <stdio.h>

// swap() function rewritten to use pointers
void swap(int *pa, int *pb) { // when used within a function, the * character(known as the unary * operator) acts as part of a pointer declarator, indicating that the parameter is a pointer to an object or function of a specific type. in this case both *pa and *pb are pointers to the int type
    int t = *pa;
    *pa = *pb; // dereferencing the pointer *pa, and rewritting it with the reference to pointer *pb
    *pb = t;
    return;
}

int main(void) {
    int a = 21;
    int b = 17;
    printf("before swap: a = %d, b = %d\n", a, b);
    swap(&a, &b); // note the use of the ampersand & character(unary & operator, also known as the address-of operator), this is necessary when referencing pointers as arguments
    printf("main: a = %d, b = %d\n", a, b);
    return 0;
}

