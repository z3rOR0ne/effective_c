#include <stdio.h>
#include <stdbool.h>

_Bool flag1 = 0;
bool flag2 = false;
_Bool flag3 = 1;
bool flag4 = true;

int main(void) {
    printf("Boolean flag1 : %d \n", flag1);
    printf("Boolean flag2 : %d \n", flag2);
    printf("Boolean flag3 : %d \n", flag3);
    printf("Boolean flag4 : %d \n", flag4);

    return 0;
}

/* Prints Out:
Boolean flag1 : 0 
Boolean flag2 : 0 
Boolean flag3 : 1 
Boolean flag4 : 1 
*/
