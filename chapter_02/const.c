#include <stdio.h>

// A brief demonstration of how you cannot reassign a constant value

int *func(int i) {
    const int j = i; // ok
    static int k = j; // error: initializer element is not constant
    return &k;
}

int main(void) {
    *func(1);
}
