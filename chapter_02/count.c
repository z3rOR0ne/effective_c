#include <stdio.h>

// --- A NOTE ON LIFETIME --- //
// In C, Objects have a storage duration that determines their lifetime.
// Four storage durations are vailable: automatic, static, thread, and allocated.
// You've alredy seen that objects of automatic storage duration are declared within a block or function parameter. This can often be conflated with scope, but it is important to diffferentiate scope as a code region whereas lifetime is a literal time that the object has to "live" in that the object will store a certain value not within a certain block of code, but prior, during, or after a file is executed.
// In the case of static storage duration, the value of the object is initialized before program startup. It also lasts as long as the entire execution of the program.
// -------------------------- // 

void increment(void) {
    static unsigned int counter = 0;
    counter ++;
    printf("%d ", counter);
}

int main(void) {
    for (int i = 0; i < 5; i++) {
        increment();
    }
    return 0;
}
