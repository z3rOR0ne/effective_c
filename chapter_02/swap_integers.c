#include <stdio.h>

// Function that swaps two integers
void swap(int a, int b) {
    int t = a; // t stores the original value of a...
    a = b; // then the value of a is changed to the value of b
    b = t; // and then the value of b is changed to the original value of a
    printf("swap: a = %d, b = %d\n", a, b); // note the syntax here, a and b are arguments ot printf(), that are referenced by %d
}

int main(void) {
    int a = 21;
    int b = 17;

    swap(a, b);
    // Note that we did not return the values of a and b to the scope of our main() function, so while swap is called and prints out the swapped values, we then call printf below, which shows that a and b still hold their original values.
    printf("main: a = %d, b = %d\n", a, b);
    return 0;
}

