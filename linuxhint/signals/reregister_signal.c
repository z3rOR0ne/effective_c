#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void sig_handler(int signum) {
    printf("\nInside handler function\n");
    signal(SIGINT, SIG_DFL); // re register signal handler for default action
}

int main() {
    signal(SIGINT, sig_handler); //register signal handler
    for (int i = 1; ; i++) { // infinite loop
        printf("%d : Inside main function\n", i);
        sleep(2); // delay for 1 second
    }
    return 0;

}
