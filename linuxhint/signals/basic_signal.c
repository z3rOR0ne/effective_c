#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void sig_handler(int signum) {
    // return type of the handler function should be void
    printf("\nInside handler function\n");
}

int main() {
    signal(SIGINT, sig_handler); // register signal handler
    for (int i = 1; ; i++) { // infinite loop
        printf("%d : Inside main function\n", i);
        sleep(1); // delay for 1 second
    }
    return 0;
}
