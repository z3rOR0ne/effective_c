#include <stdio.h>
#include <unistd.h>
#include <signal.h>

int main() {
    signal(SIGINT, SIG_IGN); // register signal handler for ignoring the signal

    for (int i = 1; ; i++) { // infinite loop
        printf("%d : Inside main function\n", i);
        sleep(1); // delay for 1 second
    }
    return 0;
}
