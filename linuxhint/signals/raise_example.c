#include <stdio.h>
#include <signal.h>

void sig_handler(int signum) {
    printf("Inside handler function\n");
}

int main() {
    signal(SIGUSR1, sig_handler); // register signal handler
    printf("Inside main function\n");
    raise(SIGUSR1);
    printf("Inside main function\n");
    return 0;
}

// OUTPUTS:
// Inside main function
// Inside handler function
// Inside main function
