#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sig_handler_parent(int signum) {
    printf("Parent : Received a response signal from child \n");
}

void sig_handler_child(int signum) {
    printf("Child : Received a signal from parent \n");
    sleep(1);
    kill(getppid(), SIGUSR1);
}

int main() {
    pid_t pid;
    if ((pid=fork())<0) {
        printf("Fork Failed\n");
        exit(1);
    }
    /* Child Process */
    else if (pid == 0) {
        signal(SIGUSR1, sig_handler_child); // register signal handler
        pause();
    }
    /* Parent Process */
    else {
        signal(SIGUSR1, sig_handler_parent); // register signal handler
        sleep(1);
        printf("Parent: sending signal to Child\n");
        kill(pid, SIGUSR1);
        printf("Parent: waiting for response\n");
        pause();
    }
    return 0;
}

// OUTPUTS:
//
// Child : Received a signal from parent 
// Parent: sending signal to Child
// Parent: waiting for response
// Parent : Received a response signal from child
