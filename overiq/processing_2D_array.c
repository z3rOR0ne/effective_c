#include <stdio.h>
#define ROW 3
#define COL 3

int main()
{
    int arr[ROW][COL], i, j;
    // First for loop gets input from the user and inserts them into our 1-d array.
    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            printf("Enter arr[%d][%d]: ", i, j); // starts off at [0][0], and loops through each element of the array, ends at arr[2][3]
            // scanf() looks for input from the user(usually text), it uses the same placeholders as printf.
            // in this case it's looking for a digit from the address of arr[i][j]
            // note the use of pointer syntax here, as this means that it has to look at the memory address to get the value of what's in the array.
            scanf("%d", &arr[i][j]);
        }
    }

    printf("\nEntered 2-d array is: \n\n");
    // Second for loop prints the elements of the 2-d array like a matrix
    for(i = 0; i < ROW; i++)
    {
        for(j = 0; j < COL; j++)
        {
            // note the use of %3d, which has the effect of printing the digits side by side (up until the 4th element, at index 3, is printed, then a new line is printed)...
            printf("%3d ", arr[i][j] );
        }
        printf("\n");
    }
    // signal to operating system everything works fine
    return 0;
}
